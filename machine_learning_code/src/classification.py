from sklearn.svm import LinearSVC
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn.cross_validation import train_test_split

def get_classification_methods(cls):
    subclasses = {}
    work = [cls]
    while work:
        parent = work.pop()
        for child in parent.__subclasses__():
            if child not in subclasses:
                subclasses[child.__name__] = child
                work.append(child)
    return subclasses

# SUPER CLASS #
class Classification(object):
    
    # Superclass: Classification

    pass

###############

# SUB CLASSES #
class SVC(Classification, LinearSVC):
    # Subclass: Support Vector Machines Classifier
    # - using Linear Support Vector Classification
    default_params = {
        'C': 1.0,
        'dual': False,
        'max_iter': 1000,
    }


class RandomForest(Classification, RandomForestClassifier):
    # Subclass: Random Trees Classifier - using Random Forest
    default_params = {
        'n_estimators': 100,
        'bootstrap': True,
        'oob_score': True,
        'n_jobs': 2,
    }

    def fit(self, *args, **kwargs):
        super(RandomForest, self).fit(*args, **kwargs)
        self.coef_ = self.feature_importances_
        return self

class ExtremelyRandomizedTrees(Classification, ExtraTreesClassifier):
    # Subclass: Random Trees Classifier - using Extremely Randomized Trees
    default_params = {
        'n_estimators': 100,
        'bootstrap': True,
        'oob_score': True,
        'n_jobs': 2,
    }

    def fit(self, *args, **kwargs):
        super(ExtremelyRandomizedTrees, self).fit(*args, **kwargs)
        self.coef_ = self.feature_importances_
        return self
